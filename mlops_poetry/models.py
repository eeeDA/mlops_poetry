from typing import Optional, Tuple

import joblib
from catboost import CatBoostRegressor
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score


class RegressionFit:
    def __init__(
        self,
        type: Optional[str] = None,
        model_path: str = "./test_model",
        to_load: bool = True,
        verbose: int = 1000,
        custom_metrics: Tuple[str] = ("R2", "MAPE"),
    ):
        """
        :param type: model type ['linreg', 'catboost']
        :param model_name: name of model
        :param to_load: if need to load model
        """

        if to_load:
            self.model = joblib.load(model_path)
            self.is_trained = True
        else:
            err_message = "Type should be catboost or linreg"
            assert type in ["linreg", "catboost"], err_message

            if type == "catboost":
                self.model = CatBoostRegressor(
                    verbose=verbose, custom_metric=custom_metrics
                )
            elif type == "linreg":
                self.model = linear_model.LinearRegression()

            self.is_trained = False

        self.path = model_path

    def fit(self, X_train, y_train) -> None:
        """
        param X_train: Training data.
        param y_train: Target values.
        """

        self.model.fit(X_train, y_train)
        joblib.dump(self.model, self.path)
        self.is_trained = True

    def predict_and_calculate_metrics(
        self, X_test, y_test=None
    ) -> Tuple[list[float], Optional[dict]]:
        """
        :param X_test: Samples to predict values.
        :param y_test: Target values to evaluate model quality.
        :return:
            y_pred – model prediction,
            dict('model_name', 'mse', 'r2_score')
        """
        assert self.is_trained, "Your model is not trained"

        y_pred = self.model.predict(X_test)

        if y_test is None:
            return y_pred, None

        mse_ = mean_squared_error(y_test, y_pred)
        r2_score_ = r2_score(y_test, y_pred)
        metrics = {"mse": [mse_], "r2_score": [r2_score_]}

        return y_pred, metrics
