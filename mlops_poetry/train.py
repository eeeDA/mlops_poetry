import os
import sys
from pathlib import Path

import git
import hydra
import mlflow
import pandas as pd
from omegaconf import DictConfig


# from dvc.api import DVCFileSystem


PROJECT_PATH = Path(__file__).absolute().parent
sys.path.append(PROJECT_PATH)


from mlops_poetry.models import RegressionFit  # noqa: E402


def save_model_and_log_metrics(model, signature):

    mlflow.catboost.save_model(
        model,
        "./catboost_model_mlflow/",
        signature=signature,
    )

    mlflow.log_param(
        "commit id", git.Repo(search_parent_directories=True).head.object.hexsha
    )
    mlflow.log_params(model.get_params())

    evals_result = model.get_evals_result()["learn"]

    for i, result in enumerate(
        [
            dict(zip(evals_result, t, strict=True))
            for t in zip(*evals_result.values(), strict=True)
        ]
    ):
        mlflow.log_metrics(result, i)


@hydra.main(config_path="../configs", config_name="config", version_base="1.3")
def run(cfg: DictConfig) -> None:
    data_pref = cfg["data_pref"]

    linreg = RegressionFit("linreg", "./linreg_model", to_load=False)
    boost = RegressionFit("catboost", "./catboost_model", to_load=False)

    # url = "https://gitlab.com/eeeDA/mlops_poetry.git"
    # fs = DVCFileSystem(url, rev="main")
    # fs.get("data", "data", recursive=True)
    os.system("dvc pull -v")

    X_train = pd.read_csv(f"{data_pref}X_train.csv", header=None)
    y_train = pd.read_csv(f"{data_pref}y_train.csv", index_col=False, header=None)

    linreg.fit(X_train, y_train)

    sys.path = sys.path[:-1]

    mlflow.set_tracking_uri(cfg["tracking_uri"])
    mlflow.set_experiment(cfg["experiment_name"])

    with mlflow.start_run():

        boost.fit(X_train, y_train)

        signature = mlflow.models.infer_signature(X_train, y_train)

        save_model_and_log_metrics(boost.model, signature)


if __name__ == "__main__":
    run()
