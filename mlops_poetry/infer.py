import os
import sys
from pathlib import Path

import hydra
import pandas as pd
from omegaconf import DictConfig


sys.path.append(Path(__file__).absolute().parent)
from mlops_poetry.models import RegressionFit  # noqa: E402


@hydra.main(config_path="../configs", config_name="config", version_base="1.3")
def run(cfg: DictConfig) -> None:
    linreg = RegressionFit("linreg", "./linreg_model", to_load=True)
    boost = RegressionFit("catboost", "./catboost_model", to_load=True)

    data_pref = cfg["data_pref"]

    os.system("dvc pull -v")

    X_test = pd.read_csv(f"{data_pref}X_test.csv", header=None)
    y_test = pd.read_csv(f"{data_pref}y_test.csv", index_col=False, header=None)

    y_pred_linreg, metrics_linreg = linreg.predict_and_calculate_metrics(X_test, y_test)
    y_pred_boost, metrics_boost = boost.predict_and_calculate_metrics(X_test, y_test)

    pd.DataFrame({"y_pred": y_pred_linreg[:, 0]}).to_csv(f"{data_pref}_y_pred_linreg.csv")
    pd.DataFrame({"y_pred": y_pred_boost}).to_csv(f"{data_pref}_y_pred_catboost.csv")

    print("linreg")
    print(metrics_linreg)

    print("catboost")
    print(metrics_boost)


if __name__ == "__main__":
    run()
