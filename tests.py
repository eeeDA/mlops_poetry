import os
import sys
from pathlib import Path


import numpy as np
import pandas as pd
from omegaconf import DictConfig
import tritonclient.http as httpclient


sys.path.append(Path(__file__).absolute().parent)
from mlops_poetry.models import RegressionFit  # noqa: E402



def predict():
    client = httpclient.InferenceServerClient(url="localhost:8500")

    os.system("dvc pull -v")

    X_test = pd.read_csv(f"./data/X_test.csv", header=None)

    model_input = httpclient.InferInput("features", X_test.shape, datatype="FP32")
    model_input.set_data_from_numpy(X_test.to_numpy(dtype=np.float32), binary_data=True)

    response = client.infer(model_name="onnx_model", inputs=[model_input])
    predictions = response.as_numpy('predictions')

    return predictions

def test_shape():

    prediсtions = predict()

    assert prediсtions.shape == (133, 1)


def test_first_value():
    
    prediсtions = predict()

    assert prediсtions[0, 0] -  99.16057 < 1e-5



if __name__ == "__main__":
    pass
