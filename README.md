# mlops_poetry

# Task

We solve diabets problem from sklearn datasets. We compare results of linear
regression and catboost basic models. Catboost metrics you can view on mlflow
server

# Usage

To run this project you need to set up mlflow server: `mlflow ui` And then run
follwing commands:

```
python -m venv test_env
source test_env/bin/activate
curl -sSL https://install.python-poetry.org | python3 -
/root/.local/bin/poetry install
pre-commit install
pre-commit run -a
python mlops_poetry/train.py
python mlops_poetry/infer.py
```

# Triton

If you want to infer model with onnxruntime backend, you need to start triton
server with `docker-compose up` and then use files `client.py` and `test.py`.
