import os
import sys
from pathlib import Path

import hydra
import numpy as np
import pandas as pd
from omegaconf import DictConfig
import tritonclient.http as httpclient


sys.path.append(Path(__file__).absolute().parent)
from mlops_poetry.models import RegressionFit  # noqa: E402


@hydra.main(config_path="configs", config_name="config", version_base="1.3")
def run(cfg: DictConfig) -> None:
    client = httpclient.InferenceServerClient(url="localhost:8500")

    data_pref = cfg["data_pref"]

    os.system("dvc pull -v")

    X_test = pd.read_csv(f"{data_pref}X_test.csv", header=None)
    y_test = pd.read_csv(f"{data_pref}y_test.csv", index_col=False, header=None)

    model_input = httpclient.InferInput("features", X_test.shape, datatype="FP32")
    model_input.set_data_from_numpy(X_test.to_numpy(dtype=np.float32), binary_data=True)

    response = client.infer(model_name="onnx_model", inputs=[model_input])
    prediсtions = response.as_numpy('predictions')

    print(prediсtions.shape == (133, 1))
    print(prediсtions[0, 0] -  99.16057 < 1e-5)


if __name__ == "__main__":
    run()
